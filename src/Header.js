import React from 'react';
import Main from './Main';

class Header extends React.Component {
  state = {
    name: 'Hello',
    page: 'Home'
  }
  render() {
    return (

      <div className="Apps">
        <header className="App">
          <div className="BH">
            <h1 >{this.state.page}</h1>
            {/*<button onClick={() => { this.setState({ name: "bye" }) }}> Exit </button>
            <button onClick={()=>this.props.changePage("page1")}>start</button>
            <button onClick={()=>this.props.changePage("page3")}>start</button>
            <button onClick={()=>this.props.changePage("page2")}>start</button>*/}
          </div>
          <p>{this.state.name + " " }<span>{ this.props.name}</span></p>
          <nav className="Atag">
            <a className="a" href="#" onClick={() => {
              this.props.changePage("page1")
              this.setState({ page: "Home" })
            }} >Home</a>
            <a className="a" href="#" onClick={() => {
              this.props.changePage("page2")
              this.setState({ page: "About" })
            }}>About</a>
            <a className="a" href="#" onClick={() => {
              this.props.changePage("page3")
              this.setState({ page: "Servis" })
            }}>Servis</a>

              
          </nav>

        </header>
      </div>
    );
  }
}

export default Header;