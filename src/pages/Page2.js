import React from 'react'
import ReactDOM from 'react-dom';
import Toconvert from './Toconvert'
import * as myfunc from './functionToconvert'
export default class Page2 extends React.Component {
    constructor() {
        super()
        this.state = {
            type:"c",
            tmp:""
        }
    }
    handelChangecelceuos = (tmp) => {
        this.setState(
            {
                type:"c",
                tmp:tmp
            }
        )

    }
    handelChangefarenhit = (tmp) => {
        this.setState(
            {
                type:"f",
                tmp:tmp
            }
        )

    }
    render() {
        const tempartF=(this.state.type==='c')?myfunc.convert(this.state.tmp,myfunc.tofarenhit):this.state.tmp
        const tempartC=(this.state.type==='f')?myfunc.convert(this.state.tmp,myfunc.tocelceuos):this.state.tmp


        return (

            <React.Fragment>
                <Toconvert name="c" value={tempartC} onhandelChange={this.handelChangecelceuos} />
                <Toconvert name="f" value={tempartF} onhandelChange={this.handelChangefarenhit} />
            </React.Fragment>


        )
    }
}

