import React from 'react'
export  function tocelceuos(farenhit){
return  (farenhit-32) *  5 /  9;
}
export  function tofarenhit(celceuos){
    return(celceuos  *  9  /  5) +32;
}
export  function convert(temp,convert){
    const input=parseFloat(temp)
    if(Number.isNaN(input)){
        return'';
    }
    const output=convert(input);
    const rouded=Math.round(output*1000)/1000;
    return rouded.toString();

}