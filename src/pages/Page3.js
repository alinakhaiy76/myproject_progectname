import React, { Component } from 'react'
import { Switch, Route, link, BrowserRouter as Router } from 'react-router-dom'
import Test1 from './Test1'
import Test2 from './Test2'
import Error from './Error'
class page3 extends Component {
    render() {
        return (
            <Router>
                <ul>
                    <li>
                        <link to="/ap">About</link>
                    </li>
                    <li>
                        <link to="/users">Home</link>
                    </li>
                </ul>
                <Switch>
                    <Route exact path="/users" component={Test1}></Route>
                    <Route path="/ap" component={Test2}></Route>
                    <Route Component={Error}></Route>
                </Switch>

            </Router>
        )
    }



}
export default page3;
 /* constructor(props){
      super(props);
      this.state={istoggel:true};
      this.handelClick=this.handelClick.bind(this);
  }
  handelClick(){
      this.setState(prevState=>({istoggel:!prevState.istoggel}));
  }
render(){
  return(
      <button onClick={this.handelClick}>
          {this.state.istoggel?'on':'off'}
      </button>
  );
}*/