import React from 'react';
import './App.css';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import Page1 from './pages/Page1';
import Page2 from './pages/Page2';
import Page3 from './pages/Page3';
class App extends React.Component {

  state = {
    currentPage: 'page1'
  }

  changePage = (page) => {

    this.setState({ currentPage: page });
  }



  render() {

    const { currentPage } = this.state
    return (
      <div>
         
        <Header changePage={this.changePage} name="ali" />
        {
          currentPage == "page1" && <Page1 />
        }
        {
          currentPage == "page2" && <Page2 />
        }
        {
          currentPage == "page3" && <Page3 />
        }
        
        <Footer />
      </div>
    );
  }
}
export default App;
